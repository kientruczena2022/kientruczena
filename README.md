# Kiến trúc Zena

Kiến trúc Zena làm thiết kế, và chúng tôi xây dựng các công trình. Kiến trúc Zena am hiểu việc mình làm, làm đàng hoàng, sáng tạo, và hết sức mình
- Địa chỉ: Căn Số 6, Đường Số 4, cityland garden hills, Phường 5, Gò Vấp,TPHCM
- SDT: 0982819997

## Những lưu ý trong thiết kế nội thất nhà cao cấp, hiện đại

### một. Vật liệu áp dụng trong Thiết kế đương đại
các thiết bị đồ bên trong hiện đại đền rồng sử dụng những vật liệu như kính cường lực đặt xa vắng ở nhà tắm, cửa kính ở trong nhà. Đồ bên trong bậc nhất trong nhà như bàn và ghế, tủ giường,...làm từ gỗ công nghiệp MFC, MDF và HDF gọn nhẹ, môi trường phủ bóng đơn giản lau chùi.

### 2. Color trong kiến trúc đương đại
màu sắc được dùng phần nhiều được xem là số đông gam màu sáng, có tính chất hay pastel nhẹ nhàng. Nhất là màu tường thường sơn màu trắng, kem, pastel để gia công nền riêng biệt cho hầu hết gam màu sinh động hơn của đồ bên trong.

Màu đối chọi sắc, sáng màu & không phối quá đa màu ở không gian

tuy nhiên hướng dẫn bên trên là đề xuất cho gần như ai bắt đầu khởi động khá đơn giản đặt đi tới quý phái này, sẽ có tương đối nhiều căn phòng hiện đại không tốt nhất thiết tuân thủ theo hầu như chính sách trên, chủ yếu sự đột phá của nhiều bên Thiết kế tài cha làm cho không gian sẽ giả nét đẹp hiện đại Đặc biệt riêng.

### 3. Phân chia không gian trong Cấu tạo
không khí Tại mỗi căn khách, phòng bếp, nhà tắm, buống ngủ hay buồng buôn bán đc câu kết linh hoạt cùng với nhau mà vẫn không gây ảnh hưởng đến đã từng nhân tài. Ví dụ như phòng khách kiêm phòng công tác, phòng nghỉ câu kết sở hữu phòng khách.

hơn thế các căn sinh hoạt không tuyệt nhất nhất cần tách biệt hoàn toàn bằng hầu hết đường vách ngăn, tường, cửa mà đi theo đàng bầu không gian mở, thoáng được sắp đặt thông minh theo không gian căn phòng.

sở hữu mọi hộ gia đình Ở đô thành bao gồm bề mặt không đc rộng, đây chính là vấn đề cho rất nhiều đơn vị bản vẽ xây dựng sư đặt Gia Công bầu khâu khí Nhà Và Đất sắp đặt sao cho hợp lý duy nhất.

### 4. Ánh nắng
ánh sáng được xem là 1 phần không thể thiếu cho đầy đủ căn hộ hiện đại. Khi đời sống chìm ở đèn khí quá đa dạng, nhân loại càng ý định hướng đến các yếu tố tình cờ nhất Rất có thể.

một mẫu cửa sổ Ở phòng khách, buống ngủ hay chiếc giếng trời được ánh nắng phản chiếu qua hắt ánh nắng tự nhiên có tác dụng ai ai cũng cảm thấy sảng khoái, lên tinh thần công tác hơn là đa số bóng đèn điện tỏa nhiệt chớp tắt. Để đưa được không ít ánh sáng vào căn nhà, rèm cửa đền mỏng làm nên màu trắng, pastel, ít hoặc không có hoa văn.

### 5. Lựa chọn vật dụng đồ bên trong
đồ bên trong đương đại hay đồ bên trong hàng đầu tất cả Đặc điểm gọn nhẹ, giản dị, chi tiết rất đơn giản. Thiết kế của đồ nội thất hướng đến việc hiện đại nhất, đơn giản cất xếp, lau dọn nhanh chóng thỏa mãn nhu cầu nhịp sống vội vã. Rộng rãi món đồ đạc trong nhà nhà đẹp mắt tích hợp phần đông công dụng hai trong 1 vừa dành dụm bầu khâu khí & Ngân sách chi tiêu như bàn nạp năng lượng thông minh, sofa kiêm giường ngủ,...

đặc biệt hơn thế phần đông thiết bị thông minh giúp người mua hàng tiết kiệm thời gian công lao để đời sống biến thành nhẹ nhàng hơn tự động hút lớp bụi dế yêu, vật dụng rửa chén,...

### 6. Phong thủy trong Thiết kế đồ bên trong công ty hiện đại
- không nên để cửa, cầu thang đối lập cửa: vì điểm lưu ý Cấu tạo bên nội thất hiện đại yêu cầu phần nhiều bầu không gian hay liên thông với nhau dẫn đến những cửa nằm đối lập nhau. Vấn đề này được cho không tốt ở Feng Shui bởi hầu hết luồng khí lộc may chảy vào công ty, theo cửa, qua hành lang và tỏa vận khí, tài lộc, cơ hội cho gia chủ. Bên cạnh đó, nếu rất nhiều cửa đối diện Vấn đề này đang đưa hầu như luồng khí vào nhà & đi theo cửa ra bên cạnh đi theo đường thẳng liền mạch. Hoặc cầu thang sẽ dẫn khí lên ở tầng còn lại tầng xệp không đc sáng sủa, u ảm đạm.

Khắc phục Vấn đề này bằng phương pháp đặt bộ bàn tròn hoặc chậu cây chậu hoa chắn giữa hai cửa đặt hoặc tấm bình phong giữa cửa và cầu thang để hạn chế thất thoát vận khí.

- Tránh để gương trước giường ngủ, Ở đầu giường & đối diện cửa: Dù ích lợi Có thể soi gương thuận lợi, bài trí phòng đẹp nhất nhưng sẽ bị nghiêm trọng đến sức khỏe của gia chủ. Gương đối lập cửa buồng vẫn nóng bỏng phần lớn tà khí bần và đẩy luồng khí vận mệnh có lợi vốn tất cả bật dậy khỏi nhà. Thay mặt gương bởi đều món đồ pha lê, Pha Lê, đá,...là biện pháp tốt bạn nên thử.

https://zena.com.vn/

https://kientruczena.tumblr.com/

https://kientruczena.wordpress.com/

https://www.deviantart.com/kientruczena
